<?php

namespace Drupal\ym_league\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

/**
 * Custom class for thin controllers.
 *
 * @ingroup routing
 */
class LeagueOfChampionsController {

  const DRAW = 0;
  const FIRST_TEAM_WIN = 1;
  const SECOND_TEAM_WIN = 2;
  const POINTS_FOR_DRAW = 1;
  const POINTS_FOR_WIN = 3;

  public static function getResults() {
    $chmp_state = self::getChampionshipState();

    if ($chmp_state) {

      if ($chmp_state[0]->week == 0) {
        $items = self::getResultZeroWeek();
      }

      $items['theme'] = 'ym_league_league_results';
      $items['properties']['week'] = $chmp_state[0]->week;

    }
    else {
      $items['theme'] = 'ym_league_not_selected_team';
    }


    return $items;

  }

  public static function setChampionship($teams) {
    $teams = array_values($teams);

    if (is_array($teams) && count($teams) == 4) {
      $conn = Database::getConnection();
      $conn->insert('ym_league_championship_state')->fields(
        array(
          'team_1' => $teams[0],
          'team_2' => $teams[1],
          'team_3' => $teams[2],
          'team_4' => $teams[3],
        )
      )->execute();
    }

  }

  public static function playNextWeek() {
    $matches = self::defineMatches();
    $matches = self::playWeekMatches($matches);
    self::saveWeekResults($matches);
  }

  private static function saveWeekResults($matches) {

    // Handle each match.
    foreach ($matches as $match) {

      //Handle results for each team.
      foreach ($match['teams'] as $nid) {
        $team = Node::load($nid);
        $enemy_nid = array_diff($match['teams'], array($nid));
        $enemy_nid = array_shift($enemy_nid);

        if ($match['winner'] == $nid) {

          // Add points.
          if (isset($team->field_points->getValue()[0]['value'])) {
            $points = $team->field_points->getValue()[0]['value'];
            $team->set('field_points', $points + self::POINTS_FOR_WIN);
          }
          else {
            $team->set('field_points', self::POINTS_FOR_WIN);
          }

          // Increase win counter.
          if (isset($team->field_win->getValue()[0]['value'])) {
            $value = $team->field_win->getValue()[0]['value'];
            $team->set('field_draw', $value + 1);
          }
          else {
            $team->set('field_draw', 1);
          }

        }
        elseif ($match['winner'] == $enemy_nid) {

          // Increase lose counter.
          if (isset($team->field_lose->getValue()[0]['value'])) {
            $value = $team->field_lose->getValue()[0]['value'];
            $team->set('field_lose', $value + 1);
          }
          else {
            $team->set('field_lose', 1);
          }

        }
        else {

          // Add points.
          if (isset($team->field_points->getValue()[0]['value'])) {
            $points = $team->field_points->getValue()[0]['value'];
            $team->set('field_points', $points + self::POINTS_FOR_DRAW);
          }
          else {
            $team->set('field_points', self::POINTS_FOR_DRAW);
          }

          // Increase draw counter.
          if (isset($team->field_draw->getValue()[0]['value'])) {
            $value = $team->field_draw->getValue()[0]['value'];
            $team->set('field_draw', $value + 1);
          }
          else {
            $team->set('field_draw', 1);
          }
        }

        // Write team which was played in corresponding field.
        if ($match['at_home'] == $nid) {
          $played_teams_field = 'field_played_teams_home';
        }
        else {
          $played_teams_field = 'field_played_teams';
        }

        $value = $team->{$played_teams_field}->getValue();
        $results = [];

        if (!empty($value)) {
          foreach ($value as $key) {
            $results[] = Node::load($key['target_id']);
          }
        }

        array_push($results, Node::load($enemy_nid));
        $team->set($played_teams_field, $results);

        $team->save();
      }
    }
  }

  private static function defineMatches(){
    $chmp_state = self::getChampionshipState();

    if (!empty($chmp_state) && $chmp_state[0]->week == 0) {
      $matches = self::defineFirstWeekMatches($chmp_state);
    }
    else {
      $matches = self::defineWeekMatches($chmp_state);
    }

    return $matches;
  }

  private static function playWeekMatches($matches) {

    foreach ($matches as &$match) {
      $match['teams'] = array_values($match['teams']);

      // Get team chance from taxonomy.
      $team1_chance = self::getTeamChance($match['teams'][0]);
      $team2_chance = self::getTeamChance($match['teams'][1]);

      // Add advantages for team which play at home.
      if ($match['at_home'] == $match['teams'][0]) {
        $team1_chance += 15;
      }
      else {
        $team2_chance += 15;
      }

      // Play the match :)
      $team1_rand  = mt_rand(1, 100);
      $team2_rand  = mt_rand(1, 100);

      // Realise advantage.
      $chance_diff = abs($team1_chance - $team2_chance);

      if ($team1_chance > $team2_chance) {
        $team1_rand += $chance_diff;
      }
      else {
        $team2_rand += $chance_diff;
      }

      // Set play's result.
      if ($team1_rand < ($team2_rand + 5)
        && $team1_rand > ($team2_rand - 5)
      ) {
        $match['winner'] = 0;
      }
      elseif ($team1_rand > $team2_rand) {
        $match['winner'] = $match['teams'][0];
      }
      else {
        $match['winner'] = $match['teams'][1];
      }

    }

    return $matches;
  }

  private static function defineWeekMatches($chmp_state) {
    
  }

  private static function getTeamsId() {
    $chmp_state = self::getChampionshipState();
    $teams_id = [];

    if (!empty($chmp_state)) {

      for ($i = 1; $i < 5; $i++) {
        $teams_id[] = $chmp_state[0]->{'team_' . $i};
      }

      return $teams_id;
    }

  }

  private static function defineFirstWeekMatches($chmp_state) {
    $teams_id = self::getTeamsId();
    $matches = [];

    $first_teams_keys = array_rand($teams_id, 2);

    foreach ($first_teams_keys as $key) {
      $matches[1]['teams'][] = $teams_id[$key];
    }

    $matches[2]['teams'] = array_diff($teams_id, $matches[1]['teams']);

    foreach ($matches as $play_number => $match) {
      $team_id_home = array_rand($matches[$play_number]['teams'], 1);
      $matches[$play_number]['at_home'] = $matches[$play_number]['teams'][$team_id_home];
    }

    return $matches;
  }

  public static function truncateChampionship() {
    $teams_id = self::getTeamsId();

    $fields = [
      'field_played_teams',
      'field_played_teams_home',
      'field_points',
      'field_draw',
      'field_lose',
      'field_win',
    ];

    foreach ($teams_id as $id) {
      $team = Node::load($id);

      foreach ($fields as $field) {
        unset($team->{$field});
      }

      $team->save();
    }

    $conn = Database::getConnection();
    $conn->truncate('ym_league_championship_state')->execute();
  }

  public static function getChampionshipState() {
    $results = &drupal_static(__METHOD__);

    if (!$results) {
      $conn = Database::getConnection();
      $results = $conn->select('ym_league_championship_state', 'ylcs')
        ->fields('ylcs')
        ->execute()
        ->fetchAll(\PDO::FETCH_OBJ);
    }

    if (count($results) > 0) {
      return $results;
    }

    return FALSE;
  }

  private function getMatchesState(){
    $conn = Database::getConnection();
    $results = $conn->select('ym_league_matches_state', 'ylcs')
      ->fields('ylcs', array('*'))
      ->execute()
      ->fetchAll(\PDO::FETCH_OBJ);

    return $results;
  }

  protected static function getResultZeroWeek() {
    $teams_id = self::getTeamsId();

    foreach ($teams_id as $nid) {
      $team = Node::load($nid);
      $items['teams'][$nid] = [
        'team' => $team->title->getValue()[0]['value'],
        'points' => 0,
        'matches' => 0,
        'on_a_visit' => 0,
        'home' => 0,
        'win' => 0,
        'draw' => 0,
        'lose' => 0,
        'prediction' => '',
        'chance' => self::getTeamChance($nid),
      ];
    }

    return $items;
  }

  private static function getTeamChance($team_id) {
    $team = Node::load($team_id);
    $chance_term_id = $team->field_team_level->getValue();
    $chance_term = Term::load($chance_term_id[0]['target_id']);
    $chance = $chance_term->field_level->getValue()[0]['value'];
    return $chance;
  }

}
