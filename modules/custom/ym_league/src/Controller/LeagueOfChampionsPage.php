<?php

namespace Drupal\ym_league\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

/**
 * Custom class for thin controllers.
 *
 * @ingroup routing
 */
class LeagueOfChampionsPage extends ControllerBase {

  /**
   * Page callback method for '/league_of_the_champions' path.
   *
   * @return array
   *   Data to be rendered.
   */
  public function leaguePage() {

    $build = [];
    $items = LeagueOfChampionsController::getResults();

    $build['league_result'] = [
      '#theme' => $items['theme'],
      '#items' => $items,
    ];

    return $build;
  }

  /**
   * Page callback method for '/finish_championship' path.
   */
  public function finishChampionship() {
    drupal_set_message('Hello bubon', 'status');
    LeagueOfChampionsController::truncateChampionship();
    return $this->redirect('ym_league.league_of_champions_page');
  }

  /**
   * Page callback method for '/play_next_week' path.
   */
  public function playNextWeek() {
    drupal_set_message('Play Next Week', 'status');
    LeagueOfChampionsController::playNextWeek();
    return $this->redirect('ym_league.league_of_champions_page');
  }

}
