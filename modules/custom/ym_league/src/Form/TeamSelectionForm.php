<?php

namespace Drupal\ym_league\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\ym_league\Controller\LeagueOfChampionsController;

/**
 * Form with team selection.
 */
class TeamSelectionForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ym_league_team_selection_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Get all node objects with type team.
    $nids = \Drupal::entityQuery('node')
      ->condition('type', 'team')
      ->execute();
    $nodes = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadMultiple($nids);

    $select_list = array();

    // Prepare select list for select form element.
    foreach ($nodes as $node) {
      $tid = $node->get('field_team_level')->getValue();
      $term = Term::load($tid[0]['target_id']);
      $value = $node->title->value . ' (';
      $value .= $term->name->value . ')';
      $select_list[$node->nid->value] = $value;
    }

    $form['team_selection'] = array(
      '#type' => 'select',
      '#title' => t('Please select 4 teams for championship'),
      '#options' => $select_list,
      '#required' => TRUE,
      '#multiple' => TRUE,
      '#ajax' => array(
        'callback' => array($this, 'validateTeamCountAjax'),
        'event' => 'change',
        'progress' => array(
          'type' => 'throbber',
          'message' => t('Checking your teams...'),
        ),
      ),
      '#suffix' => '<div class="team-selection-message"></div>',
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Start'),
      '#attributes' => array(
        'class' => array('team-selection-submit'),
      ),
    );

    // Add custom ccs library to form.
    $form['#attached']['library'][] = 'ym_league/ym_league';

    return $form;
  }

  /**
   * Validates that user selected 4 team.
   */
  public function validateTeamCountAjax(array &$form, FormStateInterface $form_state) {
    $is_submit_visible = \Drupal::state()->get('ym_league_is_submit_visible');

    $results = $form_state->getValues();
    $team_count = count($results['team_selection']);

    $response = new AjaxResponse();

    if ($team_count != 4) {
      $message = $this->t('Please select 4 teams, you have chosen @count teams', array('@count' => $team_count));
      $message_css = array('color' => 'red');

      if ($is_submit_visible) {
        $submit_css = array('visibility' => 'hidden');
        $response->addCommand(new CssCommand('.team-selection-submit', $submit_css));
        \Drupal::state()->set('ym_league_is_submit_visible', FALSE);
      }

    }
    else {
      $message = $this->t('Ok, now you can start a championship.');
      $message_css = array('color' => 'green');

      $submit_css = array('visibility' => 'visible');
      \Drupal::state()->set('ym_league_is_submit_visible', TRUE);
      $response->addCommand(new CssCommand('.team-selection-submit', $submit_css));
    }

    $response->addCommand(new CssCommand('.team-selection-message', $message_css));
    $response->addCommand(new HtmlCommand('.team-selection-message', $message));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $results = $form_state->getValues();
    $team_count = count($results['team_selection']);

    if ($team_count != 4) {
      $form_state->setErrorByName('team_selection', $this->t('You should select only 4 teams.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $is_chmp_set = LeagueOfChampionsController::getChampionshipState();

    if (!$is_chmp_set) {
      $results = $form_state->getValues();
      LeagueOfChampionsController::setChampionship($results['team_selection']);
      return $this->redirect('ym_league.league_of_champions_page');
    }
    drupal_set_message('Sorry, teams have already chosen.', 'warning');

  }

}
