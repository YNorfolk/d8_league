<?php

/**x
 * @file
 * Contains \Drupal\ym_league\Form\YmLeagueTeamSelectionForm.
 */

namespace Drupal\ym_league\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use \Drupal\Core\Ajax\CssCommand;

/**
 * Form with team selection.
 */
class LeagueOfChampionsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ym_league_league_of_champions_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['league_banner'] = array(
      '#type' => 'markup',
      '#title' => t('Please select 4 teams for championship'),
      '#markup' => $this->t('Hello world!'),
    );

    $form['next_week'] = array(
      '#type' => 'button',
      '#value' => $this->t('Next week'),
      '#weight' => 5,
    );

    $form['play_all'] = array(
      '#type' => 'button',
      '#value' => $this->t('Play all matches'),
      '#weight' => 15,
    );

    $form['start_new'] = array(
      '#type' => 'button',
      '#value' => $this->t('Start new championship'),
      '#weight' => 10,
    );

    // Add custom ccs library to form.
    $form['#attached']['library'][] = 'ym_league/ym_league';

    return $form;
  }

  /**
   * Validates that user selected 4 team.
   */
  public function validateTeamCountAjax(array &$form, FormStateInterface $form_state) {
    $is_submit_visible = \Drupal::state()->get('ym_league_is_submit_visible');

    $results = $form_state->getValues();
    $team_count = count($results['team_selection']);

    $response = new AjaxResponse();

    if ($team_count != 4) {
      $message = $this->t('Please select 4 teams, you have chosen @count teams', array('@count' => $team_count));
      $message_css = array('color' => 'red');

      if ($is_submit_visible) {
        $submit_css = array('visibility' => 'hidden');
        $response->addCommand(new CssCommand('.team-selection-submit', $submit_css));
        \Drupal::state()->set('ym_league_is_submit_visible', FALSE);
      }

    }
    else {
      $message = $this->t('Ok, now you can start a championship.');
      $message_css = array('color' => 'green');

      $submit_css = array('visibility' => 'visible');
      \Drupal::state()->set('ym_league_is_submit_visible', TRUE);
      $response->addCommand(new CssCommand('.team-selection-submit', $submit_css));
    }

    $response->addCommand(new CssCommand('.team-selection-message', $message_css));
    $response->addCommand(new HtmlCommand('.team-selection-message', $message));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
